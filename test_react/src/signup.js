import React from 'react';
import firebase from './fire';
export class SignupPage extends React.Component {

    constructor() {
        super();

        this.state =
            {
                username: '',
                email: '',
                password: '',
            }
        this.signup = this.signup.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }



    handleChange(e) {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value });

    }

    signup(e) {

        e.preventDefault();
        if (document.getElementById("logpassword").value === document.getElementById("logpasswordrepeat").value) {
            console.log("Signup with:" + this.state.email + " , " + this.state.password);
            firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((u) => {
                this.props.signupsaveStateToLocal(u);
            }).catch((error) => {
                console.log(error);
            });
        }
        else{
         alert("Please input same password and password repeat!!");
        }
    }


    render() {

        return (
            <div>

                <h1 className="signup-h1">SignUp</h1>
                <div className="textbox">
                    <i className="fas fa-user dot"></i>
                    <input className="logininput" type="text" onChange={this.handleChange} name="email" placeholder="Email"></input>
                </div>
                <div className="textbox">
                    <i className="fas fa-lock dot"></i>
                    <input id="logpassword" className="logininput" type="password" onChange={this.handleChange} name="password" placeholder="password"></input>
                </div>
                <div className="textbox">
                    <i className="fas fa-lock dot"></i>
                    <input id="logpasswordrepeat" className="logininput" type="password" onChange={this.handleChange} name="password" placeholder="Repeat your password"></input>
                </div>
                <button type="submit" onClick={this.signup} className="loginbtn">SignUp</button>
                <button className="faceloginbtn">SignUp with Facebook</button>

            </div>
        );

    }
}