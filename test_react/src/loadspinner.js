import React from 'react';
export class Loadspinner extends React.Component {
    render() 
    {
        return (  
            <div className="sk-cube-grid">
            <div className="sk-cube sk-cube1"></div>
            <div className="sk-cube sk-cube2"></div>
            <div className="sk-cube sk-cube3"></div>
            <div className="sk-cube sk-cube4"></div>
            <div className="sk-cube sk-cube5"></div>
            <div className="sk-cube sk-cube6"></div>
            <div className="sk-cube sk-cube7"></div>
            <div className="sk-cube sk-cube8"></div>
            <div className="sk-cube sk-cube9"></div>
            <div className="sk-cube sk-cube10"></div>
            <div className="sk-cube sk-cube11"></div>
            <div className="sk-cube sk-cube12"></div>
            <div className="sk-cube sk-cube13"></div>
            <div className="sk-cube sk-cube14"></div>
            <div className="sk-cube sk-cube15"></div>
            <div className="sk-cube sk-cube16"></div>
          </div>
        )
    }
}