import React from 'react';
export class ListForm extends React.Component{
    constructor()
    {
        super();
        this.addTitle=this.addTitle.bind(this);
    }
   addTitle(e)
   {
       e.preventDefault();
       const inp = document.getElementById(this.props.myTable+'-input');
       const val = inp.value;
       if(val.replace(/ /g,'').length !== 0){
       this.props.addTitle(val,this.props.myTable);
       document.getElementById(this.props.myTable+'-form').reset();
       }
   }
    render() {
        return (
          
            <form id = {this.props.myTable +"-form"} className="list-element" onSubmit={this.addTitle}>
             <input id={this.props.myTable +"-input"} className="inputbtn" autoComplete="off" placeholder=" Başlık Ekle"></input>
             <button type ="button" className="add-btn" onClick= {this.addTitle} >
             <i className="fas fa-plus"></i>
             </button>
            </form>
        
        );
      }
}