import React from 'react';
import { Exporter } from './exporter'
import '../../style.css';
import tinygradient from 'tinygradient'
import { Nav, Col, Row, ButtonDropdown, DropdownItem, DropdownToggle, DropdownMenu, Button } from 'reactstrap';
import { AppSwitch } from '@coreui/react'
export class HeatGraffer extends React.Component {
  constructor() {
    super();

    this.state =
      {
        selectlistbtn: false,
        optionslistbtn: false,
        heatmapshowswitch: false,
      }
    this.addPoint = this.addPoint.bind(this);
    this.addTitle = this.addTitle.bind(this);
    this.removeTitle = this.removeTitle.bind(this);
    this.closeWindow = this.closeWindow.bind(this);
    this.subPoint = this.subPoint.bind(this);
    this.rulertext = this.rulertext.bind(this);
    this.changeShowingTable = this.changeShowingTable.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.toggleheatmapshowswitch = this.toggleheatmapshowswitch.bind(this);


  }

  toggleselectlistbtn() {
    this.setState({
      selectlistbtn: !this.state.selectlistbtn
    });
  }
  toggleoptionslistbtn() {
    this.setState({
      optionslistbtn: !this.state.optionslistbtn
    });

  }

  toggleheatmapshowswitch() {
    this.setState({
      heatmapshowswitch: !this.state.heatmapshowswitch
    });

  }

  removeTitle(e) {
    e.preventDefault();
    this.stopPropagation(e);
    this.props.removeTitle(e.target.parentNode.parentNode.id);
  }
  addTitle(e) {
    e.preventDefault();
    this.props.addTitle("New Title", this.props.myTable);
  }


  componentDidMount() {
    this.CalAvrage();
  }
  CalAvrage() {
    var sum_val = 0;
    var result = this.props.myTitles.filter(elem => (elem.table === this.props.myTable));
    result.forEach(elem => {
      var numdata_tmp = this.getnumdataready(elem);
      sum_val += numdata_tmp;
    });

    return sum_val / (result.length);

  }

  changeShowingTable(e) {
    e.preventDefault();
    this.props.changeShowingTable(e.target.innerHTML);


  }

  subPoint(e) {
    e.preventDefault();
    this.props.addPoint(e.target.parentNode.id, -1, this.props.istimerelated);
  }

  rulertext() {
    if (this.props.myPers === "pos")
      return (<p className="avg-text">Avg: {parseInt(this.CalAvrage(), 10)}</p>)
    else if (this.props.myPers === "neg")
      return (<p className="avg-text">  Avg: {parseInt(this.CalAvrage(), 10)}  ---------------- 0</p>)

  }

  stopPropagation(e) {
    console.log("done");
    e.preventDefault();
    e.nativeEvent.stopImmediatePropagation();
    e.stopPropagation();


  }


  handleChange(e) {
    e.preventDefault();
    this.props.changeTitle(e.target.parentNode.parentNode.id, this.props.myTable);
  }

  closeWindow() {
    this.props.closeWindow(this.props.id);
  }
  addPoint(e) {
    this.props.addPoint(e.target.parentNode.id, +1, this.props.istimerelated);
  }


  addnewitem(c) {
    const addnewitem = this.addTitle;
    return (
      <div>
        <div onClick={addnewitem} className="gr-heat-square" style={{ backgroundColor: "lightgrey", height: 8 + "rem", width: 8.9 + "rem", }}  >
          <p className="chart-number"><br />+</p>
        </div>
      </div>
    )
  }


  getnumdataready(elem) {

    if (this.props.istimerelated && !this.state.heatmapshowswitch)
      if (elem.numdata === undefined || elem.numdata[this.props.date.year] === undefined || elem.numdata[this.props.date.year][this.props.date.month] === undefined ||
        elem.numdata[this.props.date.year][this.props.date.month][this.props.date.day] === undefined) {
        return (0);
      }
      else {
        return (elem.numdata[this.props.date.year][this.props.date.month][this.props.date.day]);
      }
    else {
      var sum = 0, year, month, day;
      for (year in elem.numdata) {
        for (month in elem.numdata[year]) {
          for (day in elem.numdata[year][month]) {
            sum += elem.numdata[year][month][day];

          }
        }
      }
      return (sum);
    }
  }




  SortList(list) {
    let sortedlist = [];

    sortedlist.push(list[0]);

    list.map((elem, i) => {

      const c = sortedlist.length;

      for (let a = 0; a < c; a++) {
        if (sortedlist[a].numdata > elem.numdata) {
          sortedlist.splice(a, 0, elem);
          // console.log("added"+elem);
          return;
        }
      }
      //make sorting algorithm here
    });

    sortedlist = sortedlist.reverse();
    // console.log(sortedlist);
    return sortedlist;
  }

  render() {

    console.log(this.state.heatmapshowswitch);


    var c = 0;
    var con = this.CalAvrage() * 3;
    const pers = this.props.myPers;



    function clamp(min, max, num) {
      if (num === null)
        return;
      return Math.min(Math.max(num, min), max);
    };

    function CalColor(par) {

      //var gradient = tinygradient('rgb(40,40,240)','rgb(255,255,0)','rgb(240,240,50)','rgb(255,20,20)','rgb(200,20,20)','rgb(150,20,20)');
      // var gradient = tinygradient('rgb(10,215,245)','rgb(0,255,255)', 'rgb(250,255,50)', 'rgb(255,255,0)','rgb(255,0,0)','rgb(200,0,0)','rgb(150,0,0)');
      var gradient = tinygradient('rgb(10,215,245)', 'rgb(255,255,0)', "rgb(255,128,0)", 'rgb(255,0,0)', 'rgb(200,0,0)');

      var colorarr = gradient.rgb(1000);


      if (con === 0)
        con = 1;

      if (pers === "pos") {
        // return "rgba("+600*par/con*2+","+ 400/3*par/con*2+","+par/con*200/3*2 +",1)";
        // return "rgba("+50*par/con+","+500*par/con+","+  500*par/con +",1)";
        // console.log(parseInt(par*900/con));
        return colorarr[parseInt(clamp(0, 999, par * 999 / con))];


      }
      else {
        if (par !== 0)
          // return "rgba("+220*con/(par*2)+","+ 40*con/(par*2)+","+con/(par*2)*20 +",1)";
          return "rgba(" + 30 * con / (par) + "," + con / (par) + "," + con / (par) + ",1)";
        else
          return "rgba(" + 220 * con / 1 + "," + 40 * con / 1 + "," + con / 1 * 20 + ",1)";
      }

    }


    let mySortedTitles = [];

    this.props.myTitles.map((elem, ) => {
      if (elem.table !== this.props.myTable)
        return;
      mySortedTitles.push(elem);
      c++;
    });

    mySortedTitles = this.SortList(mySortedTitles);



    const items = this.props.myTitles.map((elem, i) => {
      var numdata_tmp = this.getnumdataready(elem);
      if (elem.table !== this.props.myTable)
        return;
      let Title_id = "title_" + i;
      var rcolor = CalColor(numdata_tmp);
      var rstyle = {
        color: 'white',
        backgroundColor: rcolor,

        height: 8 + "rem",
        width: 8.9 + "rem",
      };
      return (
        <div key={i} id={Title_id}>
          <div className="gr-heat-square" style={rstyle} onContextMenu={this.subPoint} onClick={this.addPoint} >
            <input autoComplete="off" id={this.props.myTable + "-title_" + i} onClick={this.stopPropagation} className="chart-inner-text" onChange={this.handleChange} value={elem.title}></input>
            <p className="chart-number">{numdata_tmp}</p>
            <i className="arrow-triange bottomright"></i>
            <div onClick={this.removeTitle} className="arrow-triangle bottomright"></div>
            <i className="chart-remove-btn fas fa-times"></i>
          </div>
        </div>)

    });

    let heatmapshowswitch =
      <div>
      </div>

    if (this.props.istimerelated) {
      heatmapshowswitch =
        <div>
          <AppSwitch className={'mr-3 float-right ml-1 '} variant={'pill'} color={'primary'} id={"heatmapshowswitchs"} onClick={this.toggleheatmapshowswitch} />
          <h5 className="bold float-right">Show all values:</h5>
        </div>
    }


    const dropitems = this.props.myObj.map((elem, i) => {

      if (elem.type === "table") {
        return (

          <DropdownItem key={i} className="drop-content-btn" onClick={this.changeShowingTable}>{elem.name}</DropdownItem>
        )
      }
      else
        return null;
    });
    return (
      <div className="mainer">

        <Nav className="ml-auto" navbar>
          <Row>
            <Col xs="4">
              <ButtonDropdown className="ml-2" isOpen={this.state.selectlistbtn} toggle={() => { this.toggleselectlistbtn(); }}>
                <DropdownToggle caret className="chart-list">
                  <span className="chart-list">{this.props.myTable + " (Heat Chart)"}</span>
                </DropdownToggle>
                <DropdownMenu>
                  {dropitems}
                </DropdownMenu>
              </ButtonDropdown>
            </Col>
            <Col xs="6">
              {heatmapshowswitch}
            </Col>
            <Col xs="2">
              <ButtonDropdown className="ml-2 float-right" isOpen={this.state.optionslistbtn} toggle={() => { this.toggleoptionslistbtn(); }}>
                <DropdownToggle caret className="chart-list">
                  <span className="chart-list"><span className="fas fa-cog"></span> Options</span>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem className="btn-primary p-2 w-100" ><span className="fas fa-sort-amount-up"></span> Sort by Value</DropdownItem>
                  <DropdownItem className="m-0 p-0"><Exporter from={"Options"} myTable={this.props.myTable} myObj={this.props.myObj} myTitles={this.props.myTitles} /></DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </Col>
          </Row>
        </Nav>
        <br />
        <div className="heatmap-wrapper" >
          {items}
          {this.addnewitem(c)}
        </div>
        <div className="color-ruler">
          {this.rulertext()}
        </div>
      </div>
    );
  }
}