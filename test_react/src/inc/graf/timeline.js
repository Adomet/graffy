import React, { Component } from 'react'
import "./Tline.css"

const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];
export class Timeline extends Component {
    constructor() {
        super();

        this.decrementlookingmonthindex = this.decrementlookingmonthindex.bind(this);
        this.incrementlookingmonthindex = this.incrementlookingmonthindex.bind(this);
        this.changelookingdayindex = this.changelookingdayindex.bind(this);
    }



    decrementlookingmonthindex() {
        this.props.decrementlookingmonthindex();


    }
    incrementlookingmonthindex() {
        this.props.incrementlookingmonthindex();

    }
    changelookingdayindex(e) {
        e.preventDefault();
        this.props.changelookingdayindex(e);

    }
    render() {
        function getDaysOfMonth(year, month) {
            return new Date(year, month+1, 0).getDate();
        }

        let daysinmonth = getDaysOfMonth(this.props.date.year, this.props.date.month);

        let Days = [];

        for (let i = 1; i < daysinmonth + 1; i++) {
            Days.push(i);
        }

        let d = new Date();
        let month_tmp = this.props.date.month;
        let year_tmp = this.props.date.year;

        const Dayboxes = Days.map((i) => {
            if (i === d.getDate() && month_tmp === d.getMonth() && year_tmp === d.getFullYear()) {


                if (i === this.props.date.day) {
                    return (
                        <button key={i} id={i} onClick={this.changelookingdayindex} className="timeline-dayboxes-today">{i}</button>
                    )
                }
                else {

                    return (
                        <button key={i} id={i} onClick={this.changelookingdayindex} className="timeline-dayboxes-today-selected">{i}</button>
                    )
                }

            }

            if (i === this.props.date.day) {
                return (
                    <button key={i} id={i} onClick={this.changelookingdayindex} className="timeline-dayboxes-selected">{i}</button>
                )
            }
            else {
                return (
                    <button key={i} id={i} onClick={this.changelookingdayindex} className="timeline-dayboxes">{i}</button>
                )
            }
        });
        return (
            <div className="timeline-wrapper">
                <div className="timeline-month">
                    <button className="timeline-left-arrow" onClick={this.decrementlookingmonthindex}><span className="fas fa-chevron-circle-left"></span></button><button onClick={this.incrementlookingmonthindex} className="timeline-right-arrow"><span className="fas fa-chevron-circle-right"></span></button>
                    <p>{monthNames[((this.props.date.month % 12 + 12)) % 12] + " " + this.props.date.year}</p>

                </div>
                <div className="timeline-month-days">
                    {Dayboxes}
                </div>
            </div>
        )
    }
}

