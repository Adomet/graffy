import React from "react";
import ReactExport from "react-data-export";
import { Button } from 'reactstrap';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;


export class Exporter extends React.Component {

    PrepareData(List) {

        let list = [];
        let elem_tmp;
        this.props.myTitles.map((elem) => {

            if (elem.table === List.name) {

                elem_tmp = Object.create(elem);
                elem_tmp.title;
                elem_tmp.days;
                elem_tmp.daysnumdata;

                var sum = 0, year, month, day;
                for (year in elem_tmp.numdata) {
                    for (month in elem.numdata[year]) {
                        for (day in elem.numdata[year][month]) {
                            elem_tmp.days = (year + "." + month + "." + day);
                            elem_tmp.daysnumdata = (elem.numdata[year][month][day]);

                            list.push({ title: elem.title, date: elem_tmp.days, daysnumdata: elem_tmp.daysnumdata });

                        }
                    }
                }



            }
        });



        return list;
    }





    render() {
        var result = this.props.myObj.filter(elem => (elem.name === this.props.myTable));
        if (this.props.from === "Options") {
            const exportlist = result.map((elem, i) => {
                return (
                    <ExcelSheet key={i} data={this.PrepareData(elem)} name={elem.name}>
                        <ExcelColumn label="Title" value="title" />
                        <ExcelColumn label="Numdata" value="daysnumdata" />
                        <ExcelColumn label="Date" value="date" />
                    </ExcelSheet>
                )
            });
            return (
                <ExcelFile element={<p className="btn-success p-2 w-100"><span className="fas fa-file-import"></span> Export List</p>}>
                    {exportlist}
                </ExcelFile>
            );
        }


        else {
            const exportlist = this.props.myObj.map((elem, i) => {
                return (
                    <ExcelSheet key={i} data={this.PrepareData(elem)} name={elem.name}>
                        <ExcelColumn label="Title" value="title" />
                        <ExcelColumn label="Numdata" value="daysnumdata" />
                        <ExcelColumn label="Date" value="date" />
                    </ExcelSheet>
                )
            });
            return (
                <ExcelFile element={<p className="btn-success p-2 w-100"><span className="fas fa-file-import"></span> Export</p>}>
                    {exportlist}
                </ExcelFile>
            );
        }
    }
}