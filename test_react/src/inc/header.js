import React from 'react';
export class Header extends React.Component{
    constructor()
    {
        super();
        this.addTable=this.addTable.bind(this);
    }

    

    addTable(e)
   {
       e.preventDefault();
       const inp = document.getElementById("header-table-input");
       const val = inp.value;
       
       if(val.replace(/ /g,'').length !== 0 )
       {
       this.props.addTable(val);
       document.getElementById('header-form').reset();
        }
    }
    render() {
        var user = this.props.user.email;
        return (
            <div className="header">
                <form id="header-form" onSubmit={this.addTable}>
                    <input id="header-table-input" className="header-input" autoComplete="off" placeholder="+ Add New List"></input>
                </form>
                <div className="header-add-btn-wrapper">
                <button type="button" className="header-add-btn" onClick={this.addTable}>
                    <i className="fas fa-plus"></i>
                </button>


                </div>


                <div className="graffy_logo-wrapper">
                    <h1 className="graffy_logo">Graffy</h1>
                </div>
                <div className="header-import-btn-wrapper">
                    <div className="useremailtext">{user}</div>
                    <button onClick={this.props.logout} className="header-add-btn" >
                    <i className="fas fa-door-open"></i>
                    </button>
                </div>
            </div>
        );
      }
}