import React from 'react';
export class List extends React.Component {
  constructor() {

    super();
    this.state =
      { shape: false };

    this.addChart = this.addChart.bind(this);
    this.removeTitle = this.removeTitle.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.closeWindow = this.closeWindow.bind(this);
    this.addPoint = this.addPoint.bind(this);
    this.subPoint = this.subPoint.bind(this);
    this.changeTableName = this.changeTableName.bind(this);
    this.changeListPerspective = this.changeListPerspective.bind(this);
    this.changeShape = this.changeShape.bind(this);
    this.addTitle = this.addTitle.bind(this);
    this.editListTitle = this.editListTitle.bind(this);
    this.removeList = this.removeList.bind(this);
    this.shapetriangeicon = this.shapetriangeicon.bind(this);
    this.charttriangeicon = this.charttriangeicon.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.changePoint = this.changePoint.bind(this);
  }
  closeWindow() {
    this.props.closeWindow(this.props.id);
  }
  changePoint(e) {
    e.preventDefault();
    this.props.changePoint(e.target.parentNode.parentNode.id, e.target.value);
  }
  addPoint(e) {
    e.preventDefault();
    this.props.addPoint(e.target.parentNode.parentNode.id, +10);
  }
  subPoint(e) {
    e.preventDefault();
    this.props.addPoint(e.target.parentNode.parentNode.id, -10);
  }
  addChart() {
    this.props.addChart(this.props.myTable);
  }
  changeListPerspective() {
    this.props.changeListPerspective(this.props.myTable);
    let backgroundiv = document.getElementById("toggle-button-background-" + this.props.myTable);
    let buttonself = document.getElementById("toggle-button-" + this.props.myTable);
    if (backgroundiv.className === "list-g-button-background") {
      backgroundiv.className = "list-r-button-background";
      buttonself.className = "list-toggle-button right";
    }
    else {
      backgroundiv.className = "list-g-button-background";
      buttonself.className = "list-toggle-button left";
    }
  }
  removeTitle(e) {
    this.props.removeTitle(e.target.parentNode.parentNode.id);
  }

  handleChange(e) {
    e.preventDefault();
    this.props.changeTitle(e.target.parentNode.parentNode.id, this.props.myTable);

  }
  changeTitle(e) {
    e.preventDefault();
    this.props.changeTitle(e.target.parentNode.id, this.props.myTable);

  }
  changeTableName(e) {
    e.preventDefault();
    this.props.changeTableName(this.props.id, this.props.myTable);
  }
  changeShape() {
    this.setState({ shape: !this.state.shape });
  }
  addTitle(e) {
    e.preventDefault();
    this.props.addTitle("New Title", this.props.myTable);
    this.setState({ shape: true });
  }
  editListTitle(e) {
    e.preventDefault();
    this.props.toggleEditModal(this.props.id, this.props.myTable, true);

    //send prop id to change list title
  }
  removeList(e) {
    e.preventDefault();
    this.props.removeList(this.props.id);
  }
  shapetriangeicon() {

    if (this.state.shape)
      return (<i className="nav-icon icon-note"></i>)
    else
      return (<i className="nav-icon icon-note"></i>)
  }

  charttriangeicon() {

    if (this.props.activelist === this.props.myTable)
      return (<i className="arrow-left"></i>)
    else
      return (null)
  }


  getnumdataready(elem) {

    if (this.props.istimerelated)
      if (elem.numdata === undefined || elem.numdata[this.props.date.year] === undefined || elem.numdata[this.props.date.year][this.props.date.month] === undefined ||
        elem.numdata[this.props.date.year][this.props.date.month][this.props.date.day] === undefined) {
        return (0);
      }
      else {
        return (elem.numdata[this.props.date.year][this.props.date.month][this.props.date.day]);
      }
    else {
      var sum = 0, year, month, day;
      for (year in elem.numdata) {
        for (month in elem.numdata[year]) {
          for (day in elem.numdata[year][month]) {
            sum += elem.numdata[year][month][day];

          }
        }
      }
      return (sum);
    }
  }






  render() {

    let list_of_element;
    const myTablename = this.props.myTable;
    this.props.myObj.forEach(element => {
      if (element.name === myTablename) {
        list_of_element = element;
      }
    });


    let date_tmp = this.props.date;
    let myTable_tmp = this.props.myTable;
    let shape_tmp = this.state.shape;
    const items = this.props.myTitles.map((elem, i) => {


      if ((elem.table === myTable_tmp) && shape_tmp || false) {
        let Title_id = "title_" + i;
       
          return (
            <div key={i} id={Title_id} className="list-element">
              <form onSubmit={this.changeTitle}>
                <input autoComplete="off" id={this.props.myTable + "-title_" + i} className="title" onChange={this.handleChange} value={elem.title} autoFocus></input>
                <button type="button" onClick={this.removeTitle} className="add-btn"><span className="list-title-remove-btn fas fa-trash-alt"></span></button>
                <p type="number"  className="numdata" >{this.getnumdataready(elem)}</p>
              </form>
            </div>
          )
        
      }
      else return null;
    });
    return (
      <li className={this.state.shape ? ("nav-item nav-dropdown open ") : ("nav-item nav-dropdown")}>
        <div className="list-title-wrapper nav-link nav-dropdown-toggle">
          <div onClick={this.changeShape} className={"title-wrapper list-btn "}>
            <p className="title-btn title-h2">{this.props.myTable}</p>
          </div>

          {this.charttriangeicon()}


          <i onClick={this.removeList} className="add-title-btn fas fa-trash-alt"></i>
          <i onClick={this.editListTitle} className="add-title-btn fas fa-edit"></i>
          <i onClick={this.addTitle} className="add-title-btn fas fa-plus"></i>
        </div>
        <div className="list-container">
          {items}
        </div>

        {/*  <div className="list-title-wrapper nav-link nav-dropdown-toggle">
          <div onClick={this.changeShape} className={"title-wrapper list-btn "}>
            <p className="title-btn title-h2">{this.props.myTable}</p>
          </div>
          <div className="list-title-btns-wrapper ">
            {this.charttriangeicon()}
            <i onClick={this.addTitle} className="add-title-btn fas fa-plus"></i>
            <i onClick={this.addTitle} className="add-title-btn fas fa-edit"></i>
            <i onClick={this.addTitle} className="add-title-btn fas fa-trash-alt"></i>
          </div>
        </div>
        <div className="list-container">
          {items}
        </div>

          --------------------------------------------------------------------------------------------------------------------------------------
           <input  onSubmit={this.changeTableName} autoComplete="off" id ={this.props.myTable+"-table"} type ="text"className="title-h2" defaultValue={this.props.myTable}></input>
          
          <button className="list-close-btn far fa-trash-alt" onClick={this.removeTitle}>
                     </button>
          <button onClick={this.closeWindow} className="close-btn window-close-btn">
          <i className="fas fa-times"></i>
          </button>
          <button onClick={this.addChart} className="generate-chart-btn">G</button>
          
          
          
          <div id={"toggle-button-background-"+this.props.myTable} onClick={this.changeListPerspective}  className="list-g-button-background">
          <div id={"toggle-button-"+this.props.myTable} className="list-toggle-button left">


          </div>
          </div>
            
          <ListForm myTable={this.props.myTable} addTitle={this.props.addTitle}/>
          */}
      </li>
    );
  }
}