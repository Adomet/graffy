//<script src="https://www.gstatic.com/firebasejs/5.3.0/firebase.js"></script>

import firebase from 'firebase'

var config = { /* COPY THE ACTUAL CONFIG FROM FIREBASE CONSOLE */
  apiKey: "AIzaSyAoX2ApYw0Fb0I5lzM_zWzG99WKlCTDc0o",
  authDomain: "graffy-8e393.firebaseapp.com",
  databaseURL: "https://graffy-8e393.firebaseio.com",
  projectId: "graffy-8e393",
  storageBucket: "graffy-8e393.appspot.com",
  messagingSenderId: "29606662147"
};
var fire = firebase.initializeApp(config);
const faceprovider = new firebase.auth.FacebookAuthProvider();
var googleprovider = new firebase.auth.GoogleAuthProvider();
export default fire ; export {faceprovider,googleprovider};