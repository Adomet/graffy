import React from 'react';
import firebase from './fire';
export class LoginPage extends React.Component {

    constructor() {
        super();

        this.state =
            {
                email: '',
                password: '',
            }
        this.login = this.login.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }



    handleChange(e) {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value });

    }

    login(e) {
        e.preventDefault();
        console.log("login with: "+this.state.email + " , " + this.state.password);
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u) => {
            console.log(u.user.uid);
        }).catch((error) => {
            console.log(error);
        });
    }


    render() {
        
        return (
            <div>
                    <h1 className="login-h1">Login</h1>
                    <div className="textbox">
                        <i className="fas fa-user dot"></i>
                        <input className="logininput" type="text" onChange={this.handleChange} name="email" placeholder="email"></input>
                    </div>
                    <div className="textbox">
                        <i className="fas fa-lock dot"></i>
                        <input className="logininput" type="password" onChange={this.handleChange} name="password" placeholder="password"></input>
                    </div>
                    <button type="submit" onClick={this.login} className="loginbtn">Login</button>

                    <button className="faceloginbtn">Login with Facebook</button>
                    <button onClick={this.props.signinWindowSwitch} className ="signuplink">Don't have an account?</button>
            </div>
        );
    
    }
}