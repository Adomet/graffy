import React, { Component } from 'react';
import { List } from './inc/list';
import { Header } from './inc/header';
import { HeatGraffer } from './inc/graf/heatGraffer';
import firebase from './fire';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { WelcomePage } from './home';


class Main extends Component {



  constructor() {



    super();


    this.state =
      {

        myObj: [
          { name: "Test", type: "table", pers: "pos" },
        ],
        myTitles: [
          { table: "Test", title: "Matematik", numdata: 200 },
        ],
        heattable: "Test",
        user: {}
      };


    this.addTitle = this.addTitle.bind(this);
    this.removeTitle = this.removeTitle.bind(this);
    this.addPoint = this.addPoint.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.addTable = this.addTable.bind(this);
    this.closeWindow = this.closeWindow.bind(this);
    this.addChart = this.addChart.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.changeTableName = this.changeTableName.bind(this);
    this.changeListPerspective = this.changeListPerspective.bind(this);
    this.loadfrom = this.loadfrom.bind(this);
    this.changeShowingTable = this.changeShowingTable.bind(this);
    this.logout = this.logout.bind(this);
    this.saveStateToLocal = this.saveStateToLocal.bind(this);
    this.signupsaveStateToLocal = this.signupsaveStateToLocal.bind(this);


  }
  componentDidMount() {

    this.authListener();
    //this.saveStateToLocal();
  }


  authListener() {
    firebase.auth().onAuthStateChanged((user) => {

      if (user) {
        this.setState({ user },() =>{this.loadfrom()});
      }
      else {
        this.setState({ user: null });
      }
    })
  }


  loadfrom() {
    console.log("Loading");
    let db = firebase.database();
    for (let key in this.state) {
      if (key !== "user") {
        db.ref("users").child(this.state.user.uid).child(key).once('value', snap => {
        this.setState({ [key]: snap.val().key })
         })
      }
    }
    console.log("Loaded");
  }

  logout() {
    firebase.auth().signOut();
  }

  addTitle(val, table_name) {
    let updatedList = this.state.myTitles;
    updatedList.push({ table: table_name, title: val, numdata: 0 });
    this.setState({ myTitles: updatedList });
    this.saveStateToLocal();
  }

  saveStateToLocal() {
    console.log("Saved to useremail :" + this.state.user.email);
    let db = firebase.database();
    for (let key in this.state) {
      if (key !== "user") {
        console.log(key);
        db.ref("users").child(this.state.user.uid).child(key).set({ key: this.state[key] });
      }

    }

  }

  changeShowingTable(list_name) {
    this.setState({ heattable: list_name });
  }

  addTable(val) {
    let updatedList = this.state.myObj;
    let tf = true;
    updatedList.map((elem) => {
      if (elem.name === val)
        tf = false;
      return null;
    });
    if (tf) {
      updatedList.push({ name: val, type: "table", pers: "pos" });
      this.setState({ myObj: updatedList });
    }
    else
      alert("Aynı isimli tablo girdiniz!Lütfen tablo isimini değiştiriniz.");

    this.saveStateToLocal();


  }

  removeTitle(Title_id) {
    Title_id = Title_id.replace('title_', '');
    let updatedList = this.state.myTitles;
    updatedList.splice(Title_id, 1);
    this.setState({ myTitles: updatedList }, () => { this.saveStateToLocal(); this.forceUpdate(); });

  }
  //make Remove Table func

  closeWindow(id) {
    let updatedList = this.state.myObj;
    updatedList[id].activity = false;
    this.setState({ myObj: updatedList });


  }

  addPoint(Title_id, point) {

    Title_id = Title_id.replace('title_', '');
    let updatedList = this.state.myTitles;
    updatedList[Title_id].numdata += point;
    if (updatedList[Title_id].numdata < 0)
      updatedList[Title_id].numdata = 0;
    this.setState({ myTitles: updatedList });
    this.saveStateToLocal();
  }

  changeTitle(Title_id, table_name) {
    Title_id = Title_id.replace('title_', '');
    let val = document.getElementById(table_name + '-title_' + Title_id).value;
    let updatedList = this.state.myTitles;
    if (val.replace(/ /g, '').length !== 0) {
      updatedList[Title_id].title = val;
      this.setState({ myTitles: updatedList });
    }
    else {

      updatedList[Title_id].title = "";
      this.setState({ myTitles: updatedList });
    }
    document.getElementById(table_name + '-title_' + Title_id).value = updatedList[Title_id].title;
    this.saveStateToLocal();

  }

  changeListPerspective(table_name) {
    let updatedList = this.state.myObj;
    updatedList.map((elem, i) => {
      if (elem.name === table_name) {
        if (elem.pers === "pos") { elem.pers = "neg"; }
        else { elem.pers = "pos" }
      }
      return null;
    });

    this.setState({ myObj: updatedList });
    this.saveStateToLocal();



  }

  changeTableName(Title_id, table_name) {
    let val = document.getElementById(table_name + '-table').value;
    if (val.replace(/ /g, '').length !== 0) {
      let updatedTable = this.state.myObj;
      let updatedList = this.state.myTitles;

      updatedTable[Title_id].name = val;
      updatedTable.map((elem, i) => {
        if (elem.type === "chart" || elem.name === table_name)
          updatedTable[i].name = val;

        return null;
      });
      updatedList.map((elem, i) => {

        if (elem.table === table_name)
          updatedList[i].table = val;
        return null;
      });
      this.setState({ myTitles: updatedList });
      this.setState({ myObj: updatedTable });
      this.saveStateToLocal();
    }
  }
  Main() {

    const lists = this.state.myObj.map((elem, i) => {
      return (
        <List key={i} id={i} activelist={this.state.heattable} title_len={this.state.myTitles.length} changeTableName={this.changeTableName} myPers={elem.pers} changeListPerspective={this.changeListPerspective} addChart={this.addChart} myTitles={this.state.myTitles} addPoint={this.addPoint} myTable={elem.name} addTitle={this.addTitle} closeWindow={this.closeWindow} removeTitle={this.removeTitle} changeTitle={this.changeTitle} />
      )
    });
    return (
      <div className="wrapper">
        <Header user={this.state.user} logout={this.logout} addTable={this.addTable} />
        <div className="main-wrapper">
          <div className="list">
            {lists}
          </div>
          <HeatGraffer closeWindow={this.closeWindow} myObj={this.state.myObj} changeShowingTable={this.changeShowingTable} myTitles={this.state.myTitles} myTable={this.state.heattable} myPers={"pos"} addPoint={this.addPoint} />
        </div>
      </div>
    )
  }

  addChart(table_name) {
    let updatedList = this.state.myObj;
    let c = 0;
    updatedList.map((elem, i) => {
      if (elem.type === "chart")
        if (elem.name === table_name) {
          updatedList[i].activity = true;
          c++;
        }

      return null;
    });
    if (c === 0) {
      updatedList.push({ name: table_name, type: "chart", activity: true });
    }

    this.setState({ myObj: updatedList });

  }

  //main renderer
  render() {

    return (

      <div>
        {this.state.user ? (this.Main()) : (<WelcomePage signupsaveStateToLocal={this.signupsaveStateToLocal} />)}
      </div>

    )

  }
}

export default Main;


