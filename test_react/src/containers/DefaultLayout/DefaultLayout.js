import React, { Component } from 'react';
import { Loadspinner } from '../../loadspinner'
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import firebase from '../../fire';
import Dashboard from '../../views/Dashboard';
import { AppsidebarList } from '../../views/AppsidebarList'
import { AddListForm } from '../../views/AddListForm'
import { EditListForm } from '../../views/EditListForm'

import {
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
import routes from '../../routes';
import DefaultAside from './DefaultAside';
import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader';

class DefaultLayout extends Component {

  loaded = false;
  constructor() {



    super();


    this.state =
      {
        date: {
          year: new Date().getFullYear(),
          month: new Date().getMonth(),
          day: new Date().getDate(),
        },
        addlistmodal: false,
        editlistmodal: false,
        myObj: [],
        myTitles: [],
        heattable: "",
        username: "",
        user: {}
      };


    this.addTitle = this.addTitle.bind(this);
    this.removeTitle = this.removeTitle.bind(this);
    this.addPoint = this.addPoint.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.addTable = this.addTable.bind(this);
    this.addChart = this.addChart.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.changeTableName = this.changeTableName.bind(this);
    this.loadfrom = this.loadfrom.bind(this);
    this.changeShowingTable = this.changeShowingTable.bind(this);
    this.logout = this.logout.bind(this);
    this.saveStateToLocal = this.saveStateToLocal.bind(this);
    this.changePoint = this.changePoint.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleEditModal = this.toggleEditModal.bind(this);
    this.editListTitle = this.editListTitle.bind(this);
    this.removeList = this.removeList.bind(this);
    this.changelookingtime = this.changelookingtime.bind(this);
    this.decrementlookingmonthindex = this.decrementlookingmonthindex.bind(this);
    this.incrementlookingmonthindex = this.incrementlookingmonthindex.bind(this);
    this.changelookingdayindex = this.changelookingdayindex.bind(this);
  }


  componentDidMount() {

    this.setState({ user: this.props.user }, () => { this.loadfrom() });
    //this.saveStateToLocal();

  }




  decrementlookingmonthindex() {

    let date = this.state.date;
    date.month -= 1;

    if ((this.state.date.month % 12 + 12) % 12 === 0) {
      date.year -= 1;
    }

    this.setState({ date });
  }
  incrementlookingmonthindex() {

    let date = this.state.date;
    date.month += 1;

    if ((this.state.date.month % 12 + 12) % 12 === 0) {
      date.year += 1;
    }

    this.setState({ date });
  }

  changelookingdayindex(e) {
    e.preventDefault();
    let date = this.state.date;
    date.day = parseInt(e.target.id);
    this.setState({ date });
  }



  loadfrom() {
    console.log("Loading");

    let db = firebase.database();
    for (let key in this.state) {

      if (key !== "addlistmodal" && key !== "editlistmodal" && key !== "date") {
        if (key !== "user" && key !== "username") {
          db.ref("users").child(this.state.user.uid).child(key).on('value', snap => {

            if (snap.val()) {
              this.setState({ [key]: snap.val().key });
            }
            else {
              this.setState({ [key]: [] });
            }
          })
        }
        else if (key === "username") {
          db.ref("users").child(this.state.user.uid).child("username").on('value', snap => {
            this.setState({ username: snap.val() })
          })
        }
      }
    }

    console.log("Loaded");
    this.loaded = true;

  }

  logout() {
    firebase.auth().signOut();
  }

  addTitle(val, table_name) {

    var numdata_tmp = { foo: 0 };

    let updatedList = this.state.myTitles;
    updatedList.push({ table: table_name, title: val, numdata: { [this.state.date.year]: { [this.state.date.month]: { [this.state.date.day]: 0 } } } });
    this.setState({ myTitles: updatedList }, () => { this.saveStateToLocal(); });
  }

  saveStateToLocal() {
    console.log("Saved to useremail :" + this.state.user.email);
    let db = firebase.database();
    for (let key in this.state) {
      if (key !== "user" && key !== "username" && key !== "date") {
        db.ref("users").child(this.state.user.uid).child(key).set({ key: this.state[key] });
      }
    }
    this.forceUpdate();
  }

  changeShowingTable(list_name) {
    this.setState({ heattable: list_name }, () => { this.saveStateToLocal(); });
  }

  addTable(val, time_related) {

    let updatedList = this.state.myObj;
    let tf = true;
    updatedList.map((elem) => {
      if (elem.name === val)
        tf = false;
      return null;
    });
    if (tf) {
      updatedList.push({ name: val, type: "table", pers: "pos", timerelated: time_related });

      this.setState({ heattable: val });
      this.setState({ myObj: updatedList });
    }
    else
      alert("Aynı isimli tablo girdiniz!Lütfen tablo isimini değiştiriniz.");

    this.saveStateToLocal();


  }

  removeTitle(Title_id) {
    Title_id = Title_id.replace('title_', '');
    if (Title_id) {
      let updatedList = this.state.myTitles;
      updatedList.splice(Title_id, 1);
      this.setState({ myTitles: updatedList }, () => { this.saveStateToLocal(); });
    }

  }
  //make Remove Table func


  addPoint(Title_id, point, istimerelated) {



    Title_id = Title_id.replace('title_', '');
    let updatedList = this.state.myTitles;
    if (istimerelated) {

      let a = updatedList[Title_id].numdata[this.state.date.year][this.state.date.month][this.state.date.day];
      if (a !== undefined) {
        a += point;
      }
      else {
        a = 1;
      }
      if (a < 0) {
        a = 0;
      }


      updatedList[Title_id].numdata[this.state.date.year][this.state.date.month][this.state.date.day] = a;
    }

    else {

      var d = new Date();
      let a = updatedList[Title_id].numdata[d.getFullYear()][d.getMonth()][d.getDate()];
      if (a !== undefined) {
        a += point;
      }
      else {
        a = 1;
      }
      if (a < 0) {
        a = 0;
      }
      updatedList[Title_id].numdata[d.getFullYear()][d.getMonth()][d.getDate()] = a;

    }
    this.setState({ myTitles: updatedList }, () => { this.saveStateToLocal(); });


  }
  changePoint(Title_id, point) {

    Title_id = Title_id.replace('title_', '');
    let updatedList = this.state.myTitles;
    updatedList[Title_id].numdata = parseInt(point);

    if (updatedList[Title_id].numdata < 0 || !(point)) {
      updatedList[Title_id].numdata = 0;
    }
    this.setState({ myTitles: updatedList }, () => { this.saveStateToLocal(); });



  }

  changeTitle(Title_id, table_name) {
    Title_id = Title_id.replace('title_', '');
    let val = document.getElementById(table_name + '-title_' + Title_id).value;
    let updatedList = this.state.myTitles;
    if (val.replace(/ /g, '').length !== 0) {
      updatedList[Title_id].title = val;
      this.setState({ myTitles: updatedList });
    }
    else {

      updatedList[Title_id].title = "";
      this.setState({ myTitles: updatedList });
    }
    document.getElementById(table_name + '-title_' + Title_id).value = updatedList[Title_id].title;
    this.saveStateToLocal();

  }

  changeTableName(Title_id, table_name) {
    let val = document.getElementById(table_name + '-table').value;
    if (val.replace(/ /g, '').length !== 0) {
      let updatedTable = this.state.myObj;
      let updatedList = this.state.myTitles;

      updatedTable[Title_id].name = val;
      updatedTable.map((elem, i) => {
        if (elem.type === "chart" || elem.name === table_name)
          updatedTable[i].name = val;

        return null;
      });
      updatedList.map((elem, i) => {

        if (elem.table === table_name)
          updatedList[i].table = val;
        return null;
      });
      this.setState({ myTitles: updatedList });
      this.setState({ myObj: updatedTable });
      this.saveStateToLocal();
    }
  }

  addChart(table_name) {
    let updatedList = this.state.myObj;
    let c = 0;
    updatedList.map((elem, i) => {
      if (elem.type === "chart")
        if (elem.name === table_name) {
          updatedList[i].activity = true;
          c++;
        }

      return null;
    });
    if (c === 0) {
      updatedList.push({ name: table_name, type: "chart", activity: true });
    }
    this.setState({ myObj: updatedList });

  }

  removeList(List_id) {
    console.log("removed :" + List_id);
    let updatedList = this.state.myObj;
    let updatedTitles = this.state.myTitles;
    let tf = true;

    while (tf) {
      tf = false;
      updatedTitles.map((elem, i) => {
        if (elem.table === this.state.myObj[List_id].name) {
          updatedTitles.splice(i, 1);
          tf = true;
          return;
        }
      });
    }
    this.setState({ myTitles: updatedTitles });
    let a = updatedList.length;
    updatedList.splice(List_id, 1);
    this.setState({ myObj: updatedList }, () => { this.saveStateToLocal() });
    if (a !== 1) {
      console.log(updatedList);

      this.setState({ heattable: this.state.myObj[0].name });
    }
    else { this.setState({ heattable: "NO LIST" }); }
  }


  editListTitle(time_related) {
    const doc = document.getElementById("edit-list-modal-input");

    let updatedTitles = this.state.myTitles;

    updatedTitles.map((elem, i) => {

      if (elem.table === this.state.myObj[doc.name].name) {

        elem.table = doc.value;
        if (elem.date === undefined) {
          elem.date = { day: 0, month: 0, year: 0 };
          elem.date.day = parseInt(this.state.date.day);
          elem.date.month = parseInt(this.state.date.month);
          elem.date.year = parseInt(this.state.date.year);
        }
      }

    });

    this.setState({ myTitles: updatedTitles });
    let updatedList = this.state.myObj;
    updatedList[doc.name].name = doc.value;
    updatedList[doc.name].timerelated = time_related;
    this.setState({ myObj: updatedList });
    this.setState({ heattable: doc.value }, () => { this.saveStateToLocal(); })

  }


  changelookingtime(year, month, day) {
    let date = this.state.date;
    date.year = year;
    date.month = month;
    date.day = day;
    this.setState({ date });
  }




  toggleEditModal(List_id, oldlistname, con) {
    if (con) {
      this.setState({
        editlistmodal: !this.state.editlistmodal,
      }, () => { this.saveStateToLocal(); document.getElementById("edit-list-modal-input").value = oldlistname; document.getElementById("edit-list-modal-input").name = List_id; document.getElementById("editswitch").checked = true; });
    }
    else {
      this.setState({
        editlistmodal: !this.state.editlistmodal,
      }, () => { this.saveStateToLocal() });
    }

  }

  toggleModal() {
    this.setState({
      addlistmodal: !this.state.addlistmodal,
    }, () => { this.saveStateToLocal(); });

  }

  render() {
    let d = new Date(2018, 1, 0);
    console.log(d);

    //window.location.replace("/");

    if (!this.loaded) {
      return (
        <div>
          <Loadspinner />
        </div>
      )
    }
    else {

      return (
        <div className="app">
          <AddListForm addTable={this.addTable} isOpen={this.state.addlistmodal} toggleModal={this.toggleModal} />
          <EditListForm editListTitle={this.editListTitle} isOpen={this.state.editlistmodal} toggleEditModal={this.toggleEditModal} />
          <AppHeader fixed>
            <DefaultHeader heattable={this.state.heattable} myTitles={this.state.myTitles} myObj={this.state.myObj} username={this.state.username} logout={this.props.logout} />
          </AppHeader>
          <div className="app-body">
            <AppSidebar fixed display="lg">
              <AppSidebarHeader />
              <AppSidebarForm />
              <AppsidebarList date={this.state.date} removeList={this.removeList} editListTitle={this.editListTitle} toggleModal={this.toggleModal} toggleEditModal={this.toggleEditModal} state={this.state} changePoint={this.changePoint} changeTableName={this.changeTableName} changeListPerspective={this.changeListPerspective} addChart={this.addChart} myTitles={this.state.myTitles} addPoint={this.addPoint} addTitle={this.addTitle} closeWindow={this.closeWindow} removeTitle={this.removeTitle} changeTitle={this.changeTitle} />
            </AppSidebar>
            <main className="main">
              <Container fluid>
                <Dashboard date={this.state.date} changelookingdayindex={this.changelookingdayindex} incrementlookingmonthindex={this.incrementlookingmonthindex} decrementlookingmonthindex={this.decrementlookingmonthindex} myObj={this.state.myObj} addTitle={this.addTitle} removeTitle={this.removeTitle} changeShowingTable={this.changeShowingTable} myTitles={this.state.myTitles} myTable={this.state.heattable} myPers={"pos"} addPoint={this.addPoint} changeTitle={this.changeTitle} />
              </Container>
            </main>
          </div>
        </div>
      );
    }

  }
}

export default DefaultLayout;
