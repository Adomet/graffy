import React, { Component } from 'react';
import { Exporter } from '../../inc/graf/exporter'
import { Badge, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle, Button, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';
import { AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg'
import sygnet from '../../assets/img/brand/sygnet.svg'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor() {
    super();
    this.state =
      {
        userdropdonwbtn: false
      }

  }

  toggleuserdropdownbtn() {
    this.setState({
      userdropdonwbtn: !this.state.userdropdonwbtn
    });
  }


  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />

        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <p className="header-showinglist-text">{"> "+this.props.heattable}</p>
        <Nav className="ml-auto" navbar>

          <ButtonDropdown className="mr-1" isOpen={this.state.userdropdonwbtn} toggle={() => { this.toggleuserdropdownbtn(); }}>
            <DropdownToggle caret color="primary">
              <span>{this.props.username}</span>
            </DropdownToggle>
            <DropdownMenu>

              <DropdownItem className="m-0 p-0"><p className="btn-primary m-0 p-2"><span className="fas fa-user"></span> My Profile</p></DropdownItem>
              <DropdownItem className="m-0 p-0"><Exporter from={"Userbtn"} myTable={this.props.myTable} myObj={this.props.myObj} myTitles={this.props.myTitles} /></DropdownItem>
              <DropdownItem className="m-0 p-0"><p className="btn-danger m-0 p-2" onClick={this.props.logout} ><span className="fas fa-sign-out-alt"></span> Logout</p></DropdownItem>

            </DropdownMenu>
          </ButtonDropdown>
        </Nav>
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
