import React, { Component } from 'react';
import { List } from './inc/list';
import { Header } from './inc/header';
import { HeatGraffer } from './inc/graf/heatGraffer';
import firebase from './fire';
import { WelcomePage } from './welcome';
//core ui
import { HashRouter, BrowserRouter,Redirect, Route, Switch } from 'react-router-dom';
import './App.css';
// Styles
// CoreUI Icons Set
import '@coreui/icons/css/coreui-icons.min.css';
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import './scss/style.css'

import { DefaultLayout } from './containers'
// Containers
// Pages
import { Login, Page404, Page500, Register } from './views/Pages';

// import { renderRoutes } from 'react-router-config';


class App extends Component {


  constructor() {

    super();

    this.state =
      {
        user: undefined
      }
    this.logout = this.logout.bind(this);
  }

  componentWillMount() {
    this.authListener();
  }


  authListener() {
    firebase.auth().onAuthStateChanged((user) => {

      if(user)
      {
        this.setState({user});
      }
      else
      {
        this.setState({user:null});
      }

    })
  }

  logout() {
    firebase.auth().signOut();
    console.log("Logout");
  }



  login()
  {

    console.log("login" );

    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/login" name="Login Page" component={Login} />
          <Route exact path="/register" name="Register Page" component={Register} />
          <Route exact path="/404" name="Page 404" component={Page404} />
          <Route exact path="/500" name="Page 500" component={Page500} />
          <Redirect to="/login"/>
        </Switch>
      </BrowserRouter>
    )
  }
   home ()
   {
      console.log("home : " + this.state.user.email);
    return (
      <BrowserRouter>
        <Switch>
          <Redirect exact path="/login" to="/" />
          <Redirect exact path="/register" to="/" />
          <DefaultLayout user ={this.state.user} logout = {this.logout}/>
        </Switch>
      </BrowserRouter>
    )
   }

  //main renderer
  render() {

     if(this.state.user === undefined){
     return(
       <div>
         </div>
     )
    }
    else 
    {
    return(
      <div>
        {this.state.user ? (this.home()) : (this.login()) }
      </div>
    )
  }
  }
}

export default App;