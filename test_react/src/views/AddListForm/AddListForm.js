import React from 'react';
import {
    Badge,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row, Button, Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import { AppSwitch } from '@coreui/react'
export class AddListForm extends React.Component {
    constructor() {
        super();

        this.addTable = this.addTable.bind(this);
    
    }



    addTable(e) {
        e.preventDefault();
        const inp = document.getElementById("add-list-modal-input");
        const val = inp.value;

        if (val.replace(/ /g, '').length !== 0) {
            this.props.addTable(val,document.getElementById("addswitch").checked);
            document.getElementById('add-list-modal-form').reset();
        }
        this.props.toggleModal();
    }



    render() {
        return (
            <div className="animated fadeIn">
                <form>
                    <Modal isOpen={this.props.isOpen} toggle={this.props.toggleModal} className={"add-list-modal modal-lg"}>
                        <ModalHeader className="add-list-modal-title" toggle={this.props.toggleModal}>Add List</ModalHeader>
                        <ModalBody>

                            <Form id="add-list-modal-form" onSubmit={this.addTable}>

                                <FormGroup>
                                    <Label>List Name:</Label>
                                    <Input autoComplete="off" id="add-list-modal-input" type="text" placeholder="Enter List's name" />
                                    <FormText className="help-block">Please enter your list's name</FormText>
                                </FormGroup>

                            </Form>
                            <Row>
                                <Col xs="2">
                                    <p className="bold">Time related:</p>
                                </Col>
                                <Col xs="1">
                                    <AppSwitch className={'mx-1 float-right'} variant={'pill'} color={'primary'} id={"addswitch"}  />
                                </Col>
                                <Col xs="3">
                                </Col>

                                <Col xs="6">
                                    <Button color="danger" className="float-right mr-1" onClick={this.props.toggleModal}>Cancel</Button>
                                    <Button color="primary" className="float-right mr-1" onClick={this.addTable} >Add</Button>
                                </Col>
                            </Row>
                        </ModalBody>
                    </Modal>
                </form>
            </div>
        )
    }
}