import React from 'react';
import { List } from '../../inc/list';
export class AppsidebarList extends React.Component {
    render() {

        const lists = this.props.state.myObj.map((elem, i) => {
                return (
                    <List key={i} id={i} date = {this.props.date} myObj = {this.props.state.myObj} removeList={this.props.removeList} editListTitle={this.props.editListTitle} toggleModal={this.props.toggleModal} toggleEditModal={this.props.toggleEditModal} changePoint={this.props.changePoint} activelist={this.props.state.heattable} title_len={this.props.state.myTitles.length} changeTableName={this.props.changeTableName} myPers={elem.pers} changeListPerspective={this.props.changeListPerspective} addChart={this.props.addChart} myTitles={this.props.state.myTitles} addPoint={this.props.addPoint} myTable={elem.name} addTitle={this.props.addTitle} closeWindow={this.props.closeWindow} removeTitle={this.props.removeTitle} changeTitle={this.props.changeTitle} />
                )
            

        });

        return (
            <div className="sidebar">
                <p className="add-list-btn "> <i className="fas fa-list-ul"></i> My Lists</p>
                <div className="scrollbar">
                    <ul className="nav">

                        {lists}

                    </ul>
                </div>
                <button onClick={this.props.toggleModal} className="sidebar-footer add-list-btn mt-auto" type="button">
                    <i className="fas fa-plus"></i> Add New List
                </button>
            </div>

        )
    }
}