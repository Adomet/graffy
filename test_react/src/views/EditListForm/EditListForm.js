import React from 'react';
import {
    Badge,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row, Button, Modal, ModalBody, ModalFooter, ModalHeader
} from 'reactstrap';
import { AppSwitch } from '@coreui/react'

let timerelationswitchtf = false;
export class EditListForm extends React.Component {
    constructor() {
        super();

        this.editListTitle = this.editListTitle.bind(this);
    }


  
    editListTitle(e) {
        this.props.editListTitle(document.getElementById("editswitch").checked);      
        this.props.toggleEditModal();
    }



    render() {

        return (
            <div className="animated fadeIn">
                <form>
                    <Modal isOpen={this.props.isOpen} toggle={this.props.toggleEditModal} className={"add-list-modal modal-lg"}>
                        <ModalHeader className="add-list-modal-title" toggle={this.props.toggleModal}>Edit List</ModalHeader>
                        <ModalBody>

                            <Form id="edit-list-modal-form" onSubmit={this.editListTitle}>

                                <FormGroup>
                                    <Label>List Name:</Label>
                                    <Input autoComplete="off" id="edit-list-modal-input" type="text" placeholder="Enter List's name" />
                                    <FormText className="help-block">Please enter your list's name</FormText>
                                </FormGroup>

                            </Form>
                            <Row>
                                <Col xs="2">
                                    <p className="bold">Time related:</p>
                                </Col>
                                <Col xs="1">
                                    <AppSwitch className={'mx-1 float-right'} variant={'pill'} color={'primary'} id="editswitch" />
                                </Col>
                                <Col xs="3">
                                </Col>

                                <Col xs="6">
                                    <Button color="danger" className="float-right mr-1" onClick={this.props.toggleEditModal}>Cancel</Button>
                                    <Button color="primary" className="float-right mr-1" onClick={this.editListTitle} >Edit</Button>
                                </Col>
                            </Row>

                        </ModalBody>
                    </Modal>
                </form>
            </div>
        )
    }
}