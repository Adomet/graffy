import React, { Component } from 'react';
import firebase from '../../../fire';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { faceprovider, googleprovider } from '../../../fire';



class Login extends Component {

  constructor() {
    super();

    this.state =
      {
        email: '',
        password: '',
      }






    this.login = this.login.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.signupFacebook = this.signupFacebook.bind(this);
    this.signupGoogle = this.signupGoogle.bind(this);

  }
  handleChange(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });

  }
  


  signupFacebook(e) {
    e.preventDefault();
    firebase.auth().signInWithPopup(faceprovider).then((result) => {
     console.log(result.user.displayName);
      this.signupFacebookGoogleSaveStateToLocal(result);
      
    }).catch((error) => {
      alert(error.massage);
    });
  }
  signupGoogle(e) {
    e.preventDefault();
    firebase.auth().signInWithPopup(googleprovider).then((result) => {
      this.signupFacebookGoogleSaveStateToLocal(result);
    }).catch((error) => {
      alert(error.massage);
    });
  }
  signupFacebookGoogleSaveStateToLocal(us) {
    let db = firebase.database();
    db.ref("users").child(us.user.uid).child("username").set(us.user.displayName);
  }


  login(e) {
    e.preventDefault();
    console.log("login with: " + this.state.email + " , " + this.state.password);
    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u) => {
      console.log(u.user.uid);
    }).catch((error) => {
      alert(error.massage);
    });
  }
  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4 m-0">
                  <CardBody>
                    <Form onSubmit={this.login}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Email" name="email" onChange={this.handleChange} autoComplete="email" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" name="password" onChange={this.handleChange} placeholder="Password" autoComplete="current-password" />
                      </InputGroup>

                      <Row>
                        <Col xs="12">
                          <Button onClick={this.login} color="primary" className="w-100 mb-1">Login</Button>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="6" className="text-right">
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="6" >
                          <Button onClick={this.signupFacebook} className="btn-facebook w-100 my-1" block><span>facebook</span></Button>
                        </Col>
                        <Col xs="6">
                          <Button onClick={this.signupGoogle} className="btn-google-plus w-100 my-1" block><span>Google +</span></Button>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="6" className="text-left">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="register-card text-white bg-primary py-5 m-0 d-md-inline">
                  <CardBody style={{ margin: 15 + '%' }} className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>If you dont have an account</p>
                      <Link to="/register"><Button color="primary" className="mt-3" active>Register Now!</Button></Link>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>

        </Container>
      </div>
    );
  }
}

export default Login;
