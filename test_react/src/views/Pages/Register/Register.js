import React, { Component } from 'react';
import firebase from '../../../fire';
import { Redirect } from 'react-router-dom';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { faceprovider, googleprovider } from '../../../fire';
class Register extends Component {
  constructor() {
    super();

    this.state =
      {
        username: '',
        email: '',
        password: '',
        repeatpassword: '',
        toDashbord: false,
      }
      this.registerdata =
      {
        myObj: [],
        myTitles: [],
      }


    this.signupEmail = this.signupEmail.bind(this);
    this.signupFacebook = this.signupFacebook.bind(this);
    this.signupGoogle = this.signupGoogle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.gologinpage = this.gologinpage.bind(this);
  }



  handleChange(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });

  }

  signupEmail(e) {

    e.preventDefault();
    if (this.state.password === this.state.repeatpassword) {
      console.log("Signup with:" + this.state.email);
      firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((u) => {
        this.signupEmailSaveStateToLocal(u).then(() => { this.setState({ toDashbord: true }); });
      }).catch((error) => {
        console.log(error);
      });
    }
    else {
      alert("Please input same password and password repeat!!");
    }

  }




  signupFacebook(e) {
    e.preventDefault();
    firebase.auth().signInWithPopup(faceprovider).then((result) => {
     console.log(result.user.displayName);
      this.signupFacebookGoogleSaveStateToLocal(result);
      
    }).catch((error) => {
      alert(error.massage);
    });
  }
  signupGoogle(e) {
    e.preventDefault();
    firebase.auth().signInWithPopup(googleprovider).then((result) => {
      this.signupFacebookGoogleSaveStateToLocal(result);
    }).catch((error) => {
      alert(error.massage);
    });
  }
  signupFacebookGoogleSaveStateToLocal(us) {
    let db = firebase.database();
    db.ref("users").child(us.user.uid).child("username").set(us.user.displayName);
    for (let key in this.registerdata) {
      db.ref("users").child(us.user.uid).child(key).set({ key: this.registerdata[key] });
    }
  }

  signupEmailSaveStateToLocal(us) {
    let db = firebase.database();
    db.ref("users").child(us.user.uid).child("username").set(this.state.username);
    for (let key in this.registerdata) {
      db.ref("users").child(us.user.uid).child(key).set({ key: this.registerdata[key] });
    }
  }




  gologinpage() {
    window.location.replace("/");
  }
  render() {
    if (this.state.toDashbord === true) {
      return (
        <Redirect to='/' />
      )
    }




    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form onSubmit={this.signupEmail}>
                    <h1>Register</h1>
                    <p className="text-muted">Create your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" onChange={this.handleChange} name="username" placeholder="Username" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input name="email" onChange={this.handleChange} type="text" placeholder="Email" autoComplete="email" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" name="password" onChange={this.handleChange} placeholder="Password" autoComplete="new-password" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" name="repeatpassword" onChange={this.handleChange} placeholder="Repeat password" autoComplete="new-password" />
                    </InputGroup>
                    <Row>
                      <Col xs="12">
                        <Button color="success w-100 my-1" block>Create Account</Button>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="12" sm="6">
                        <Button onClick={this.signupFacebook} className="btn-facebook w-100 my-1" block><span>facebook</span></Button>
                      </Col>
                      <Col xs="12" sm="6">
                        <Button onClick={this.signupGoogle} className="btn-google-plus w-100 my-1" block><span>Google +</span></Button>
                      </Col>
                    </Row>

                  </Form>
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="6" className="text-left">
                      <Button onClick={this.gologinpage} color="link" className="px-0">Already have an account?</Button>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Register;
