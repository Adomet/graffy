import React, { Component } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import { HeatGraffer } from '../../inc/graf/heatGraffer';
import { Timeline } from '../../inc/graf/timeline'
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
} from 'reactstrap';

class Dashboard extends Component {


  loadiftimerelated() {
    let returnelement;
    this.props.myObj.forEach(element => {
      if (element.name === this.props.myTable) {
        returnelement = element;
      }
    });
    return (returnelement);
  }




  render() {

    if (this.props.myObj.length === 0) {
      return null;
    }
    let myList = this.loadiftimerelated();
    if (myList === undefined) { return null }
    if (myList.timerelated) {
      return (
        <div className="animated fadeIn" >
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Timeline  date = {this.props.date}  changelookingdayindex={this.props.changelookingdayindex} incrementlookingmonthindex = {this.props.incrementlookingmonthindex} decrementlookingmonthindex = {this.props.decrementlookingmonthindex} myObj={this.props.myObj} myTitles={this.props.myTitles} myTable={this.props.myTable} />
                </CardBody>
              </Card>
              <Card>
                <CardBody>
                  <div className="chart-wrapper" >
                    <div>{this.props.heattable}</div>
                    <HeatGraffer istimerelated={myList.timerelated}  date = {this.props.date}  myObj={this.props.myObj} addTitle={this.props.addTitle} removeTitle={this.props.removeTitle} changeShowingTable={this.props.changeShowingTable} myTitles={this.props.myTitles} myTable={this.props.myTable} myPers={this.props.myPers} addPoint={this.props.addPoint} changeTitle={this.props.changeTitle} />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>

        </div>
      );
    }
    else {
      return (

        <div className="animated fadeIn" >
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <div className="chart-wrapper" >
                    <div>{this.props.heattable}</div>
                    <HeatGraffer istimerelated={false} myObj={this.props.myObj} addTitle={this.props.addTitle} removeTitle={this.props.removeTitle} changeShowingTable={this.props.changeShowingTable} myTitles={this.props.myTitles} myTable={this.props.myTable} myPers={this.props.myPers} addPoint={this.props.addPoint} changeTitle={this.props.changeTitle} />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>

        </div>
      )

    }

  }
}

export default Dashboard;
