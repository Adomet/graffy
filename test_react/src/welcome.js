import React from 'react'
import { LoginPage } from './login';
import { SignupPage } from './signup'

export class WelcomePage extends React.Component {


    constructor() {
        super();

        this.state =
            {
                signinwindow: false
            }
        this.signinWindowSwitch = this.signinWindowSwitch.bind(this);

    }
    signinWindowSwitch() {
        this.setState({ signinwindow: true });
    }



    render() {
        return (
            <div>
                <h1 className="logo">Graffy</h1>
                <div className="login_window">
                    {this.state.signinwindow ? (<SignupPage signupsaveStateToLocal={this.props.signupsaveStateToLocal}/>) : (<LoginPage signinWindowSwitch={this.signinWindowSwitch} />)}
                </div>
            </div>
        )
    }
}
